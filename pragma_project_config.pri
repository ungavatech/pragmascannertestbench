#define project specific include and lib path

#define OCC path
win32 {
    CASROOT=C:/OCC/750-vc142-x64-static
    CASROOT_THIRDPARTY=C:/OCC/750_thirdparty
    CASROOT_INCLUDES=$${CASROOT}/inc
    CASROOT_LIBS_DEBUG=$${CASROOT}/win64/vc14/libd
    CASROOT_LIBS_RELEASE=$${CASROOT}/win64/vc14/lib
    CASROOT_FREETYPE_INCLUDES=C:/OCC/750_thirdparty/freetype-2.5.5-vc14-64/include
    CASROOT_FREETYPE_LIBS=C:/OCC/750_thirdparty/freetype-2.5.5-vc14-64/lib
    CASROOT_TBB_INCLUDES=C:/OCC/750_thirdparty/tbb2018_20180411oss/include
    CASROOT_TBB_LIBS=C:/OCC/750_thirdparty/tbb2018_20180411oss/lib/intel64/vc14
}
unix {
    # support of OpenGL ES on the Embedded PragmaPro
    # NEED TO HAVE CONFIG+=PRT_EGLS define in the kit's build steps/additional arguments
    CONFIG(PRAGMA_PRO_TARGET) {
        CASROOT=/opt/OCC/750-armv7-a
        DEFINES +=HAVE_EGL HAVE_GLES2 __ANDROID__ PRT_EGLS WL_EGL_PLATFORM
        LIBS+= -lEGL -lGLESv2
    }
    else {
        CASROOT=/opt/OCC/750-gcc_64-static
    }
    CONFIG(debug, debug|release) {
        unix:DEFINES += _DEBUG
    }
    CASROOT_INCLUDES=$${CASROOT}/include/opencascade
    CASROOT_LIBS_DEBUG=$${CASROOT}/lib
    CASROOT_LIBS_RELEASE=$${CASROOT}/lib
    CASROOT_FREETYPE_LIBS=$${CASROOT}/lib
}

# related to pragmalib_global.h, will make sure nothing is exported since all
# lib are use as static lib
DEFINES+=PRAGMA_STATIC_BUILD OCCT_STATIC_BUILD
