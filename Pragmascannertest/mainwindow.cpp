#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "pragmadrive.h"

#include <QFile>
#include <QTextStream>
#include <QtNetwork>


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    ui->driveBox->addItem("Applied Motion Stepper via Ethernet");
    ui->driveBox->addItem("Applied Motion Servo via Ethernet");
    ui->driveBox->addItem("JirehScanLink via USB");

    ui->axisBox->addItem("X");
    ui->axisBox->addItem("Y");

}


MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_connectButton_clicked()
{
    pragma::DriveType drive_type;
    if (ui->driveBox->currentText() == "Applied Motion Stepper via Ethernet")
    {
        drive_type = pragma::kAppMotionStepper;
    }

    else if (ui->driveBox->currentText() == "JirehScanLink via USB")
    {
        drive_type = pragma::kJireh;
    }

    else if (ui->driveBox->currentText() == "Applied Motion Servo via Ethernet")
    {
        drive_type = pragma::kAppMotionServo;
    }

    else
    {
        qDebug() << "Drive selected is not supported" <<  ui->driveBox->currentText();
    }


    drive.Initialize(drive_type);

    int x_port = ui->XaxisPortEdit->text().toInt();
    int y_port = ui->YaxisPortEdit->text().toInt();
    QString x_address = ui->XaxisAddressEdit->text();
    QString y_address = ui->YaxisAddressEdit->text();

    std::vector<drive_address> address;
    address.resize(2);

    address[xAxis].port_number = x_port;
    address[xAxis].ip_address = x_address;
    address[yAxis].port_number = y_port;
    address[yAxis].ip_address = y_address;

    drive.Connect(address);
    qDebug() << "Drive connected" <<  ui->driveBox->currentText();

}


void MainWindow::on_directionButton_clicked()
{
    drive.InvertDirection(active_axis);
}


void MainWindow::on_moveToButton_clicked()
{
    QString x_str = ui->xAxisMoveToEdit->text();
    QString y_str = ui->yAxisMoveToEdit->text();

    qDebug() << "X: " <<  ui->xAxisMoveToEdit->text();
    qDebug() << "Y: " <<  ui->yAxisMoveToEdit->text();


    int x = x_str.toInt();
    int y = y_str.toInt();
    drive.MoveTo(x,y);
}

void MainWindow::on_startJogButton_clicked()
{
    drive.StartJog(active_axis);
}

void MainWindow::on_stopJogButton_clicked()
{
    drive.StopJog(active_axis);
}

void MainWindow::on_commandButton_clicked()
{
    int crawler_start = ui->crawlerStartEdit->text().toInt();
    int raster_start = ui->rasterStartEdit->text().toInt();
    int crawler_end = ui->crawlerEndEdit->text().toInt();
    int raster_end = ui->rasterEndEdit->text().toInt();
    int crawler_speed = ui->crawlerSpeedEdit->text().toInt();
    int raster_speed = ui->rasterSpeedEdit->text().toInt();
    int sweep_ival = ui->sweepIvalEdit->text().toInt();

    drive.RasterScan(crawler_start, raster_start, crawler_end, raster_end, crawler_speed, raster_speed, sweep_ival);
}


void MainWindow::on_homeButton_clicked()
{
    drive.MoveTo(0,0);

}


void MainWindow::on_axisBox_currentTextChanged(const QString &arg1)
{
    if (ui->axisBox->currentText() == "X")
    {
        active_axis = xAxis;
    }

    else if (ui->axisBox->currentText() == "Y")
    {
        active_axis = yAxis;
    }

}


void MainWindow::on_yAxisSpeedEdit_textChanged(const QString &arg1)
{
    int speed = arg1.toInt();
    // ajustement de vitesse en mm/s TODO
    speed = speed*10;
    drive.SetSpeed(yAxis, speed);
    qDebug() << "Y axis speed set to " <<  speed;
}


void MainWindow::on_xAxisSpeedEdit_textEdited(const QString &arg1)
{
    int speed = arg1.toFloat();
    // ajustement de vitesse en mm/s TODO
    speed = speed*10;
    drive.SetSpeed(xAxis, speed);
    qDebug() << "X axis speed set to " <<  speed;
}


void MainWindow::on_testButton_clicked()
{
    drive.Test();
}

void MainWindow::on_stopScanButton_clicked()
{
    drive.StopScan();
}

void MainWindow::on_setPositionButton_clicked()
{
    drive.SetPosition(xAxis);
    drive.SetPosition(yAxis);
}

void MainWindow::on_accelerationEdit_textChanged(const QString &arg1)
{
    int acceleration = arg1.toInt();
    drive.SetAcceleration(xAxis, acceleration);
    drive.SetAcceleration(yAxis, acceleration);
}

void MainWindow::on_stopButton_clicked()
{
    drive.StopJog(active_axis);
}

void MainWindow::on_stepsPerRotationEdit_textChanged(const QString &arg1)
{
    int steps = arg1.toInt();
    drive.SetStepsPerRotation(xAxis, steps);
    drive.SetStepsPerRotation(yAxis, steps);
}

void MainWindow::on_DistancePerRotationEdit_textChanged(const QString &arg1)
{
    int distance = arg1.toInt();
    drive.SetDistancePerRotation(xAxis, distance);
    drive.SetDistancePerRotation(yAxis, distance);
}
