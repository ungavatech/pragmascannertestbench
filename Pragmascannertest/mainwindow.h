#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "pragmadrive.h"


QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:

    void on_connectButton_clicked();
    void on_directionButton_clicked();
    void on_moveToButton_clicked();
    void on_startJogButton_clicked();
    void on_stopJogButton_clicked();
    void on_commandButton_clicked();
    void on_homeButton_clicked();
    void on_axisBox_currentTextChanged(const QString &arg1);
    void on_yAxisSpeedEdit_textChanged(const QString &arg1);
    void on_xAxisSpeedEdit_textEdited(const QString &arg1);
    void on_testButton_clicked();
    void on_stopScanButton_clicked();
    void on_setPositionButton_clicked();
    void on_accelerationEdit_textChanged(const QString &arg1);

    void on_stopButton_clicked();

    void on_stepsPerRotationEdit_textChanged(const QString &arg1);

    void on_DistancePerRotationEdit_textChanged(const QString &arg1);

private:
    Ui::MainWindow *ui;

    pragma::Drive drive;
    axisIndex active_axis;

};
#endif // MAINWINDOW_H
