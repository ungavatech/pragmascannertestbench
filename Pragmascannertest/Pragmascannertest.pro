
TEMPLATE = app

CONFIG += c++11

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
#pragmascannertest.depends = prt_lib/pragmascanner


# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SUBPROJET_LIBS=$$OUT_PWD/../lib

LIBS += -L$$SUBPROJET_LIBS

SOURCES += \
    main.cpp \
    mainwindow.cpp

HEADERS += \
    mainwindow.h

FORMS += \
    mainwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES +=

QT += network

INCLUDEPATH += ../pragma_lib/pragmascanner

CONFIG(debug, debug|release) {

    unix:PRE_TARGETDEPS+=$${SUBPROJET_LIBS}/libpragmascannerd.a
    win32:PRE_TARGETDEPS+=$${SUBPROJET_LIBS}/pragmascannerd.lib
    LIBS+= -lpragmascannerd
}

else {

    unix:PRE_TARGETDEPS+=$${SUBPROJET_LIBS}/libpragmascanner.a
    win32:PRE_TARGETDEPS+=$${SUBPROJET_LIBS}/pragmascanner.lib \
    LIBS+= -lpragmascanner

}
